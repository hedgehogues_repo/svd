import pandas as pd

from src.svd import SVD, WEIGHT

df = pd.read_csv('data/test.csv', index_col=0)
svd = SVD(data=df.as_matrix())
svd.optimize()
print('Mean:', svd.w[WEIGHT.MEAN])
print('Film features:', svd.w[WEIGHT.FILM_FEATURES])
print('User features:', svd.w[WEIGHT.USER_FEATURES])
print('User mean:', svd.w[WEIGHT.FILM_MEAN])
print('Film mean:', svd.w[WEIGHT.USER_MEAN])
print('Predicted matrix:', svd.predict_all_data())
print('Input matrix:', svd.data)
