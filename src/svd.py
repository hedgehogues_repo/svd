from enum import Enum
import numpy as np


class WEIGHT(Enum):
    MEAN = 0
    USER_MEAN = 1
    FILM_MEAN = 2
    USER_FEATURES = 3
    FILM_FEATURES = 4


class SVD:

    def __init__(self, data, eps=0.00001, eta_modify=0.66, t=0.5, threshold=0.3,
                 lambda0=0.05, count_features=2, eta=0.1, seed=42):
        np.random.seed(seed)
        self.eps = eps
        self.data = data
        self.lambda0 = lambda0
        self.count_features = count_features
        self.eta = eta
        self.eta_modify = eta_modify
        self.t = t
        self.threshold = threshold
        self.w = {
            WEIGHT.MEAN: 0,
            WEIGHT.USER_MEAN: np.random.randn(1, data[0].shape[0])[0],
            WEIGHT.FILM_MEAN: np.random.randn(1, data[0].shape[0])[0],
            WEIGHT.USER_FEATURES: np.random.randn(data[0].shape[0], count_features),
            WEIGHT.FILM_FEATURES: np.random.randn(data[0].shape[0], count_features)
        }

        self.rmse = [1000]
        self.old_rmse = self.rmse[-1] * 2

    def calculate_weights(self, user_key, film_key):
        err = self.data[user_key][film_key] - \
            (
                self.w[WEIGHT.MEAN] +
                self.w[WEIGHT.USER_MEAN][user_key] + self.w[WEIGHT.FILM_MEAN][film_key] +
                np.dot(self.w[WEIGHT.USER_FEATURES][user_key], self.w[WEIGHT.FILM_FEATURES][film_key])
            )
        self.rmse[-1] += np.square(err)

        self.w[WEIGHT.MEAN] += self.eta * err
        self.w[WEIGHT.USER_MEAN] += self.eta * (err - self.lambda0 * self.w[WEIGHT.USER_MEAN])
        self.w[WEIGHT.FILM_MEAN] += self.eta * (err - self.lambda0 * self.w[WEIGHT.FILM_MEAN])

        for f in range(self.count_features):
            self.w[WEIGHT.USER_FEATURES][user_key][f] += self.eta * (
                    err * self.w[WEIGHT.FILM_FEATURES][film_key][f] -
                    self.lambda0 * self.w[WEIGHT.USER_FEATURES][user_key][f]
            )
            self.w[WEIGHT.FILM_FEATURES][film_key][f] += self.eta * (
                    err * self.w[WEIGHT.USER_FEATURES][user_key][f] -
                    self.lambda0 * self.w[WEIGHT.FILM_FEATURES][film_key][f]
            )

    def optimize(self):
        step = 0
        while np.abs(self.old_rmse - self.rmse[-1]) > self.eps:
            self.old_rmse = self.rmse[-1]
            self.rmse.append(0)
            count = 0
            for user_key, user_value in enumerate(self.data):
                for film_key, film_value in enumerate(user_value):
                    if not np.isnan(film_value):
                        count += 1
                        self.calculate_weights(user_key, film_key)

            self.rmse[-1] = np.sqrt(np.divide(self.rmse[-1], count))
            print("Iteration", step, ":\tRMSE=", self.rmse[-1], "\n")
            if self.rmse[-1] > self.old_rmse - self.threshold:
                self.eta = self.eta * self.eta_modify
                self.threshold = self.threshold * self.t
            step += 1

    def predict(self, user_key, film_key):
        return self.w[WEIGHT.MEAN] + \
               self.w[WEIGHT.USER_MEAN][user_key] + self.w[WEIGHT.FILM_MEAN][film_key] + \
               np.dot(self.w[WEIGHT.USER_FEATURES][user_key], self.w[WEIGHT.FILM_FEATURES][film_key])

    def predict_all_data(self):
        answ = []
        for i in range(len(self.data)):
            answ.append([])
            for j in range(len(self.data[0])):
                answ[-1].append(self.predict(i, j))
        return np.array(answ)